# kintone Autocomplete Package

atom エディタ用の kintone の autocomplete パッケージです。  
autocomplete-plus が必須です（最新atomならすでに導入済みのはず）。  
** 自分用に作成したのをバックアップを兼ねて公開しています。 **

## kintone とは ##
サイボウズ様（ https://www.cybozu.com/jp/ ）が提供しているWEBデータベースです。  

kintone URL  
https://kintone.cybozu.com/jp/

![A screenshot of your package](https://f.cloud.github.com/assets/69169/2290250/c35d867a-a017-11e3-86be-cd7c5bf3ff9b.gif)
